# Dropwizard Listening DataSource

Do something with a database connection when it's obtained from the pool, before putting it to use in application.

Tested with dropwizard-jdbi 0.9.2.  
Not tested with dropwizard-hibernate!

## Example usage

### 2. Add maven dependency
```xml
<dependency>
    <groupId>com.bertoncelj.dropwizard</groupId>
    <artifactId>dropwizard-listening-datasource</artifactId>
    <version>1.0.0</version>
</dependency>
```

### 2. Configure JDBI to use ListeningDataSource

```java
@Override
public void run(TestAppConfig configuration, Environment environment) throws Exception {
    // an ordinary datasource provided by Dropwizard that will do all the real work
    ManagedDataSource delegateDataSource = configuration.getDatabase().build(environment.metrics(), environment.getName());

    // a wrapper around a datasource, that calls a listener, each time a connection is obtained from the pool
    ManagedDataSource dataSource = new ListeningDataSource(delegateDataSource, new ConnectionListener() {
        @Override
        public void onGetConnection(Connection connection) throws SQLException {
            log.info("Obtained database connection: {}", connection);
        }
    });

    // configure JDBI to use a specific datasource (our ListeningDataSource)
    DBI dbi = new DBIFactory().build(environment, configuration.getDatabase(), dataSource, environment.getName());

    // do something with your application...
}
```
