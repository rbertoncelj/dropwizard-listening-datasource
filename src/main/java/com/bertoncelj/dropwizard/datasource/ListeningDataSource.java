/*
Copyright 2016 Rok Bertoncelj

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.bertoncelj.dropwizard.datasource;

import io.dropwizard.db.ManagedDataSource;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * A ManagedDataSource implementation that calls {@link ConnectionListener#onGetConnection(Connection)} when
 * a database connection is obtained. All operations are delegated to a wrapped data source that must be
 * specified in constructor.
 *
 * @author Rok Bertoncelj
 */
public class ListeningDataSource implements ManagedDataSource {
    protected ManagedDataSource delegate;
    protected ConnectionListener connectionListener;


    /**
     * @param delegate           a data source that will do real work - all calls will be delegated to it
     * @param connectionListener a connection listener to be notified when a connection is taken from pool
     * @throws NullPointerException if <code>delegate</code> argument is null
     */
    public ListeningDataSource(ManagedDataSource delegate, ConnectionListener connectionListener) {
        this.delegate = delegate;
        this.connectionListener = checkNotNull(connectionListener);
    }


    @Override
    public Connection getConnection() throws SQLException {
        Connection connection = delegate.getConnection();
        connectionListener.onGetConnection(connection);
        return connection;
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        Connection connection = delegate.getConnection(username, password);
        connectionListener.onGetConnection(connection);
        return connection;
    }


    /*
        Nothing interesting below - just delegating everything to our ManagedDataSource...
     */

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return delegate.getLogWriter();
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        delegate.setLogWriter(out);
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return delegate.getLoginTimeout();
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        delegate.setLoginTimeout(seconds);
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return delegate.getParentLogger();
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return delegate.unwrap(iface);
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return delegate.isWrapperFor(iface);
    }

    @Override
    public void start() throws Exception {
        delegate.start();
    }

    @Override
    public void stop() throws Exception {
        delegate.stop();
    }
}
