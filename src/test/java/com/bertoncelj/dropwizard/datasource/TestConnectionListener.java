/*
Copyright 2016 Rok Bertoncelj

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.bertoncelj.dropwizard.datasource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Rok Bertoncelj
 */
public class TestConnectionListener implements ConnectionListener {
    private List<Connection> connectionHistory = new ArrayList<>();

    @Override
    public void onGetConnection(Connection connection) throws SQLException {
        connectionHistory.add(connection);
    }

    public List<Connection> getConnectionHistory() {
        return connectionHistory;
    }
}
