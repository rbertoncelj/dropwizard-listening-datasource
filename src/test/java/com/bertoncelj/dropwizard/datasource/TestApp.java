/*
Copyright 2016 Rok Bertoncelj

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.bertoncelj.dropwizard.datasource;

import io.dropwizard.Application;
import io.dropwizard.db.ManagedDataSource;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Environment;
import org.skife.jdbi.v2.DBI;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Rok Bertoncelj
 */
public class TestApp extends Application<TestAppConfig> {
    private List<Connection> connectionHistory = new ArrayList<>();
    private DBI dbi;

    @Override
    public void run(TestAppConfig configuration, Environment environment) throws Exception {
        // an ordinary datasource provided by Dropwizard that will do all the real work
        ManagedDataSource delegateDataSource = configuration.getDatabase().build(environment.metrics(), environment.getName());

        // a wrapper around a datasource, that calls a listener, each time a connection is obtained from the pool
        ManagedDataSource dataSource = new ListeningDataSource(delegateDataSource, new ConnectionListener() {
            @Override
            public void onGetConnection(Connection connection) throws SQLException {
                connectionHistory.add(connection);
            }
        });

        // configure JDBI to use a specific datasource
        dbi = new DBIFactory().build(environment, configuration.getDatabase(), dataSource, environment.getName());
    }

    public List<Connection> getConnectionHistory() {
        return connectionHistory;
    }

    public DBI getDbi() {
        return dbi;
    }
}
