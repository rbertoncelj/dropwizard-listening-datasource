/*
Copyright 2016 Rok Bertoncelj

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package com.bertoncelj.dropwizard.datasource;

import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.ClassRule;
import org.junit.Test;
import org.skife.jdbi.v2.Handle;

import static org.junit.Assert.*;

/**
 * @author Rok Bertoncelj
 */
public class ConnectionListenerTest {
    @ClassRule
    public static final DropwizardAppRule<TestAppConfig> RULE =
            new DropwizardAppRule<>(TestApp.class, ResourceHelpers.resourceFilePath("test.yml"));

    @Test
    public void test() throws Exception {
        TestApp testApp = RULE.getApplication();
        assertTrue(testApp.getConnectionHistory().isEmpty());

        Handle handle = testApp.getDbi().open();
        assertEquals(1, testApp.getConnectionHistory().size());
        assertSame(handle.getConnection(), testApp.getConnectionHistory().get(0));

        Handle secondHandle = testApp.getDbi().open();
        assertEquals(2, testApp.getConnectionHistory().size());
        assertSame(secondHandle.getConnection(), testApp.getConnectionHistory().get(1));

        handle.close();
        secondHandle.close();
        assertEquals(2, testApp.getConnectionHistory().size());
    }
}
